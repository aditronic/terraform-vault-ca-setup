data "hcloud_servers" "vault_cluster" {
  with_selector = "vault=server"
}

resource "vault_auth_backend" "bootstrapper" {
  type = "approle"
  path = "bootstrapper"
}

resource "vault_approle_auth_backend_role" "bootstrapper" {
  backend               = vault_auth_backend.bootstrapper.path
  role_name             = "bootstrapper_role"
  bind_secret_id        = true
  secret_id_ttl         = 86400
  secret_id_bound_cidrs = ["127.0.0.1/32"]
  token_policies = ["bootstrapper-kv-write",
    "bootstrapper-cert-generate",
    "bootstrapper-policy-write",
    "bootstrapper-token-create",
  "bootstrapper-auth-ldap"]
  token_ttl              = 60
  token_max_ttl          = 300
  token_explicit_max_ttl = 3600
}

resource "vault_approle_auth_backend_role_secret_id" "bootstrapper" {
  backend    = vault_auth_backend.bootstrapper.path
  role_name  = vault_approle_auth_backend_role.bootstrapper.role_name
  depends_on = [vault_approle_auth_backend_role.bootstrapper]
}

resource "vault_approle_auth_backend_role" "vault_cluster" {
  count                 = length(data.hcloud_servers.vault_cluster.servers)
  backend               = vault_auth_backend.bootstrapper.path
  role_name             = "vault_cluster_${data.hcloud_servers.vault_cluster.servers[count.index].labels.private_ip}"
  bind_secret_id        = true
  secret_id_ttl         = 86400
  secret_id_bound_cidrs = ["${data.hcloud_servers.vault_cluster.servers[count.index].labels.private_ip}/32"]
  token_policies = [
    "bootstrapper-cert-generate",
    "ansible-pull-hcloud",
    "ansible-pull-hcp"
  ]
  token_bound_cidrs = ["${data.hcloud_servers.vault_cluster.servers[count.index].labels.private_ip}/32"]
  token_ttl = 28800
  token_max_ttl = 86400
  token_explicit_max_ttl = 604800
}

resource "local_file" "cluster_approle" {
  count    = length(data.hcloud_servers.vault_cluster.servers)
  content  = jsonencode(vault_approle_auth_backend_role.vault_cluster[count.index])
  filename = "/var/spool/bootstrapper/${vault_approle_auth_backend_role.vault_cluster[count.index].role_name}.role_id"
}

resource "local_file" "role_id" {
  content  = vault_approle_auth_backend_role.bootstrapper.role_id
  filename = "/var/lib/misc/bootstrapper-role_id"
}

resource "local_file" "secret_id" {
  content  = vault_approle_auth_backend_role_secret_id.bootstrapper.secret_id
  filename = "/var/lib/misc/bootstrapper-secret_id"
}
