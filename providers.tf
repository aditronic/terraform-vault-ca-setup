terraform {
  required_version = "~> 1.6"

  required_providers {
    hcloud = { source = "hetznercloud/hcloud", version = "~> 1.45" }
    vault  = { source = "hashicorp/vault", version = "~> 3.23" }
    random = { source = "hashicorp/random", version = "~> 3.6" }
  }
}
