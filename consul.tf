data "hcloud_server" "bootstrapper" {
  with_selector = "bootstrapper=server"
}

resource "consul_key_prefix" "nginx_lb" {
  path_prefix = "nginx_lb/"

  subkeys = {
    "backend_prefix"                    = "bootstrapper"
    "default_domain"                    = "aditronic.se"
    "default_domain_internal"           = "aditronic.internal"
    "loadbalancer_tag"                  = "nginx_lb"
    "proxy_set_headers/Host"            = "$host"
    "proxy_set_headers/X-Forwarded-For" = "$proxy_add_x_forwarded_for"
    "proxy_set_headers/X-Real-IP"       = "$remote_addr"
  }
}

resource "consul_key_prefix" "ansible_pull" {
  path_prefix = "bootstrapper_kv/ansible_pull/"

  subkeys = {
    "vault_token" = vault_token.ansible_pull_token.client_token
    "vault_addr" = "https://${data.hcloud_server.bootstrapper.labels.private_ip}:8200"
  }
}
