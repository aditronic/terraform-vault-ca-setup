resource "vault_mount" "bootstrapper" {
  path    = "bootstrapper_kv"
  type    = "kv"
  options = { "version" = "2" }
}

resource "vault_kv_secret_backend_v2" "bootstrapper" {
  mount        = vault_mount.bootstrapper.path
  max_versions = 5
}

resource "random_password" "pulp_admin_password" {
  length           = 64
  special          = true
  override_special = "~.-_"

  lifecycle {
    ignore_changes = [length, special]
  }
}

resource "vault_kv_secret_v2" "pulp_admin_password" {
  name  = "pulp/admin_credentials"
  mount = vault_mount.bootstrapper.path
  data_json = jsonencode(
    {
      pulp_user     = "admin",
      pulp_password = random_password.pulp_admin_password.result,
      pulp_base64 = base64encode(join(":", ["admin",
      random_password.pulp_admin_password.result]))
    }
  )
}

resource "vault_kv_secret_v2" "hcloud_token" {
  name  = "hcloud/token"
  mount = vault_mount.bootstrapper.path
  data_json = jsonencode(
    {
      hcloud_token = var.hcloud_token
    }
  )
}

resource "vault_kv_secret_v2" "hcp_credentials" {
  name  = "hcp/credentials"
  mount = vault_mount.bootstrapper.path
  data_json = jsonencode(
    {
      hcp_client_id     = var.hcp_client_id
      hcp_client_secret = var.hcp_client_secret
      hcp_organization  = var.hcp_organization
      hcp_project       = var.hcp_project
    }
  )
}
