resource "vault_ldap_auth_backend" "ldap" {
  path                   = "ldap"
  url                    = var.ldap_url
  certificate            = file("/var/lib/misc/ldap_ca_chain.pem")
  userattr               = "uid"
  userdn                 = "ou=Users,${var.ldap_org_dn}"
  groupdn                = "ou=Users,${var.ldap_org_dn}"
  binddn                 = var.ldap_bind_dn
  bindpass               = var.ldap_bind_pwd
  groupfilter            = "(member={{.UserDN}})"
  groupattr              = "memberOf"
  token_ttl              = 3600
  token_max_ttl          = 14400
  token_explicit_max_ttl = 86400
}

resource "vault_ldap_auth_backend_group" "vault_groups" {
  count     = length(jsondecode(var.ldap_vault_group_policies))
  groupname = keys(jsondecode(var.ldap_vault_group_policies))[count.index]
  policies  = values(jsondecode(var.ldap_vault_group_policies))[count.index]

  depends_on = [
    vault_ldap_auth_backend.ldap
  ]
}
