resource "vault_policy" "engine_policy" {
  name = "engine-policy"

  policy = <<EOT
# Enable secrets engine
path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# List enabled secrets engine
path "sys/mounts" {
  capabilities = [ "read", "list" ]
}

# Work with pki secrets engine
path "aditronic_self_signed*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}
EOT
}

resource "vault_policy" "bootstrapper_kv_write" {
  name   = "bootstrapper-kv-write"
  policy = file("policies/bootstrapper-kv-write.policy")
}

resource "vault_policy" "bootstrapper_cert_generate" {
  name   = "bootstrapper-cert-generate"
  policy = file("policies/bootstrapper-cert-generate.policy")
}

resource "vault_policy" "bootstrapper_policy_write" {
  name   = "bootstrapper-policy-write"
  policy = file("policies/bootstrapper_policy_write.policy")
}

resource "vault_policy" "bootstrapper_token_create" {
  name   = "bootstrapper-token-create"
  policy = file("policies/bootstrapper_token_create.policy")
}

resource "vault_policy" "bootstrapper_auth_ldap" {
  name   = "bootstrapper-auth-ldap"
  policy = file("policies/bootstrapper_auth_ldap.policy")
}

resource "vault_policy" "ansible_pull_hcloud" {
  name   = "ansible-pull-hcloud"
  policy = file("policies/ansible_pull_hcloud.policy")
}

resource "vault_policy" "ansible_pull_hcp" {
  name = "ansible-pull-hcp"
  policy = file("policies/ansible_pull_hcp.policy")
}
