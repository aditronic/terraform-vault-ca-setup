data "hcloud_image" "cluster_image" {
  with_selector     = "name=${var.hcloud_install_image}"
  with_architecture = "arm"
}

data "hcloud_network" "internal" {
  name = "internal"
}

resource "random_pet" "cattle" {
  count = var.instances

  keepers = {
    image_id    = data.hcloud_image.cluster_image.id
    location    = var.location
    server_type = var.server_type
  }
}

resource "hcloud_server" "cluster_nodes" {
  count       = var.instances
  name        = random_pet.cattle[count.index].id
  server_type = random_pet.cattle[count.index].keepers.server_type
  location    = random_pet.cattle[count.index].keepers.location
  image       = random_pet.cattle[count.index].keepers.image_id

  network {
    network_id = data.hcloud_network.internal.id
    alias_ips  = []
  }

  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }
}


