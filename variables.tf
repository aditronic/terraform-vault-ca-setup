variable "vault_addr" {
  description = "The vault address to use"
  type        = string
  default     = "http://localhost:8200"
}

variable "hcloud_install_image" {
  description = "The install image to use"
  type        = string
  default     = "alma-9-arm64"
}

variable "instances" {
  description = "Number of cluster instances to create"
  type        = number
}

variable "location" {
  description = "Location of cluster instances"
  type        = string
}

variable "server_type" {
  description = "Machine type of cluster instances"
  type        = string
}

variable "ldap_vault_group_policies" {
  description = "Policies to attach to the vault ldap groups"
  type        = string
}

variable "ldap_url" {
  description = "URL to LDAP server used for vault auth method"
  type        = string
}

variable "ldap_org_dn" {
  description = "DN for the ldap organization"
  type        = string
}

variable "ldap_bind_dn" {
  description = "DN to use to bind to the LDAP server"
  type        = string
}

variable "ldap_bind_pwd" {
  description = "Password associated with ldap_bind_dn"
  type        = string
  sensitive   = true
}

variable "hcloud_token" {
  description = "Token for cluster nodes to access Hetzner Cloud"
  type        = string
  sensitive   = true
}

variable "hcp_client_id" {
  description = "Client ID used for HCP access"
  type        = string
  default     = ""
}

variable "hcp_client_secret" {
  description = "Client secret used for HCP access"
}

variable "hcp_organization" {
  description = "Organization used in HCP"
  type        = string
  default     = ""
}

variable "hcp_project" {
  description = "Project used in HCP"
  type        = string
  default     = ""
}
