resource "vault_token" "ansible_pull_token" {
  policies  = ["ansible-pull-hcloud"]
  no_parent = true
  ttl       = "60m"
  renewable = false
}

