resource "vault_mount" "pki" {
  path        = "aditronic_self_signed"
  type        = "pki"
  description = "aditronic.self-signed CA"

  default_lease_ttl_seconds = 86400
  max_lease_ttl_seconds     = 2592000
}

resource "vault_pki_secret_backend_root_cert" "pki" {
  backend     = vault_mount.pki.path
  type        = "internal"
  common_name = "aditronic.self-signed"
  ttl         = 2592000
  issuer_name = "self-signed-2024"
}

output "vault_pki_secret_backend_root_cert" {
  value       = vault_pki_secret_backend_root_cert.pki.certificate
  description = "self-signed-2024 certificate"
}

resource "local_file" "pki_cert" {
  content  = vault_pki_secret_backend_root_cert.pki.certificate
  filename = "/etc/pki/ca-trust/source/anchors/hetzner_aditronic_selfsigned.crt"
}

resource "vault_pki_secret_backend_issuer" "pki_issuer" {
  backend                        = vault_mount.pki.path
  issuer_ref                     = vault_pki_secret_backend_root_cert.pki.issuer_id
  issuer_name                    = vault_pki_secret_backend_root_cert.pki.issuer_name
  revocation_signature_algorithm = "SHA256WithRSA"
}

resource "vault_pki_secret_backend_role" "role" {
  backend          = vault_mount.pki.path
  name             = "self-signed-role"
  allow_ip_sans    = true
  key_type         = "rsa"
  key_bits         = 4096
  allow_subdomains = true
  allow_any_name   = true
}

resource "vault_pki_secret_backend_config_urls" "config_urls" {
  backend                 = vault_mount.pki.path
  issuing_certificates    = ["http://localhost:8200/v1/pki/ca"]
  crl_distribution_points = ["http://localhost:8200/v1/pki/crl"]
}

resource "vault_mount" "pki_int" {
  path        = "aditronic_self_signed_int"
  type        = "pki"
  description = "aditronic.self-signed intermediate"

  default_lease_ttl_seconds = 86400
  max_lease_ttl_seconds     = 2592000
}

resource "vault_pki_secret_backend_intermediate_cert_request" "csr_request" {
  backend     = vault_mount.pki_int.path
  type        = "internal"
  common_name = "aditronic.self-signed Intermediate Authority"
}

resource "local_file" "csr_request_cert" {
  content  = vault_pki_secret_backend_intermediate_cert_request.csr_request.csr
  filename = "/etc/pki/aditronic-self-signed/intermediate.csr"
}

resource "vault_pki_secret_backend_root_sign_intermediate" "intermediate" {
  backend     = vault_mount.pki.path
  common_name = "aditronic.self-signed"
  csr         = vault_pki_secret_backend_intermediate_cert_request.csr_request.csr
  format      = "pem_bundle"
  ttl         = 2592000
  issuer_ref  = vault_pki_secret_backend_root_cert.pki.issuer_id
}

resource "local_file" "intermediate_ca_cert" {
  content  = vault_pki_secret_backend_root_sign_intermediate.intermediate.certificate
  filename = "/etc/pki/aditronic-self-signed/intermediate.cert.pem"
}

resource "vault_pki_secret_backend_intermediate_set_signed" "intermediate" {
  backend     = vault_mount.pki_int.path
  certificate = vault_pki_secret_backend_root_sign_intermediate.intermediate.certificate
}

resource "vault_pki_secret_backend_issuer" "intermediate" {
  backend     = vault_mount.pki_int.path
  issuer_ref  = vault_pki_secret_backend_intermediate_set_signed.intermediate.imported_issuers[0]
  issuer_name = "self-signed-2024-intermediate"
}

resource "vault_pki_secret_backend_role" "intermediate_role" {
  backend          = vault_mount.pki_int.path
  issuer_ref       = vault_pki_secret_backend_issuer.intermediate.issuer_ref
  name             = "self-signed"
  ttl              = 86400
  max_ttl          = 259200
  allow_ip_sans    = true
  key_type         = "rsa"
  key_bits         = 4096
  allowed_domains  = ["aditronic.self-signed"]
  allow_subdomains = true
}

locals {
  certs = ["consul", "pulp", "vault", "redis-stack"]
}

resource "vault_pki_secret_backend_cert" "certs" {
  count = length(local.certs)

  issuer_ref  = vault_pki_secret_backend_issuer.intermediate.issuer_ref
  backend     = vault_pki_secret_backend_role.intermediate_role.backend
  name        = vault_pki_secret_backend_role.intermediate_role.name
  common_name = "${local.certs[count.index]}.aditronic.self-signed"
  ttl         = 43200
  auto_renew  = true
  revoke      = true
}

resource "local_file" "test_aditronic_self_signed_cert" {
  count           = length(local.certs)
  content         = vault_pki_secret_backend_cert.certs[count.index].certificate
  filename        = "/etc/pki/nginx/${local.certs[count.index]}.pem"
  file_permission = "0664"
}

resource "local_file" "test_aditronic_self_signed_key" {
  count           = length(local.certs)
  content         = vault_pki_secret_backend_cert.certs[count.index].private_key
  filename        = "/etc/pki/nginx/private/${local.certs[count.index]}.key"
  file_permission = "0660"
}

resource "vault_pki_secret_backend_cert" "vault_cluster_certs" {
  count       = length(data.hcloud_servers.vault_cluster.servers)
  issuer_ref  = vault_pki_secret_backend_issuer.intermediate.issuer_ref
  backend     = vault_pki_secret_backend_role.intermediate_role.backend
  name        = vault_pki_secret_backend_role.intermediate_role.name
  common_name = "vault-${data.hcloud_servers.vault_cluster.servers[count.index].name}.aditronic.self-signed"
  ip_sans     = [data.hcloud_servers.vault_cluster.servers[count.index].labels.private_ip]
  ttl         = 43200
  auto_renew  = true
  revoke      = true
}

output "vault_cluster" {
  value = data.hcloud_servers.vault_cluster
}

resource "local_file" "vault_aditronic_self_signed_cert" {
  count           = length(data.hcloud_servers.vault_cluster.servers)
  content         = vault_pki_secret_backend_cert.vault_cluster_certs[count.index].certificate
  filename        = "/etc/pki/bootstrapper-vault-cluster/${data.hcloud_servers.vault_cluster.servers[count.index].labels.private_ip}.pem"
  file_permission = "0664"
}

resource "local_file" "vault_aditronic_self_signed_key" {
  count           = length(data.hcloud_servers.vault_cluster.servers)
  content         = vault_pki_secret_backend_cert.vault_cluster_certs[count.index].private_key
  filename        = "/etc/pki/bootstrapper-vault-cluster/${data.hcloud_servers.vault_cluster.servers[count.index].labels.private_ip}.key"
  file_permission = "0660"
}

#output "vault_pki_secret_backend_cert_example_dot_com_cert" {
#  value       = vault_pki_secret_backend_cert.example_dot_com.certificate
#  description = "aditronic.self-signed certificate"
#}
#
#output "vault_pki_secret_backend_cert_example_dot_com_issuing_ca" {
#  value       = vault_pki_secret_backend_cert.example_dot_com.issuing_ca
#  description = "aditronic.self-signed issuing_ca"
#}
#
#output "vault_pki_secret_backend_cert_example_dot_com_serial_number" {
#  value       = vault_pki_secret_backend_cert.example_dot_com.serial_number
#  description = "aditronic.self-signed serial number"
#  #sensitive   = true
#}
#
#output "vault_pki_secret_backend_cert_example_dot_com_private_key_type" {
#  value       = vault_pki_secret_backend_cert.example_dot_com.private_key_type
#  description = "aditronic.self-signed private key type"
#}
#
# resource "vault_pki_secret_backend_root_cert" "root_2024" {
#   backend     = vault_mount.pki.path
#   type        = "internal"
#   common_name = "example.com"
#   ttl         = "315360000"
#   issuer_name = "root-2024"
#   key_name    = "root_2024"
# }
# 
# resource "vault_pki_secret_backend_issuer" "root_2024" {
#   backend                        = vault_mount.pki.path
#   issuer_ref                     = vault_pki_secret_backend_root_cert.root_2024.issuer_id
#   issuer_name                    = vault_pki_secret_backend_root_cert.root_2024.issuer_name
#   revocation_signature_algorithm = "SHA256WithRSA"
# }
# 
# resource "vault_pki_secret_backend_role" "role_2024" {
#   backend        = vault_mount.pki.path
#   name           = "2024-servers"
#   allow_any_name = true
# }
# 
# resource "vault_pki_secret_backend_intermediate_cert_request" "new_csr" {
#   backend     = vault_mount.pki.path
#   type        = "existing"
#   common_name = "example.com"
#   key_ref     = vault_pki_secret_backend_root_cert.root_2024.key_name
# }
# 
# resource "local_file" "new_csr_file" {
#   content  = vault_pki_secret_backend_intermediate_cert_request.new_csr.csr
#   filename = "cross-signed-intermediate.csr"
# }
# 
# resource "vault_pki_secret_backend_root_sign_intermediate" "root_2024" {
#   backend     = vault_mount.pki.path
#   csr         = vault_pki_secret_backend_intermediate_cert_request.new_csr.csr
#   common_name = "example.com"
#   ttl         = 43800
#   issuer_ref  = vault_pki_secret_backend_root_cert.root_2023.issuer_id
# }
# 
# resource "vault_pki_secret_backend_intermediate_set_signed" "root_2024" {
#   backend     = vault_mount.pki.path
#   certificate = vault_pki_secret_backend_root_sign_intermediate.root_2024.certificate
# }
# 
# output "vault_pki_secret_backend_root_sign_intermediate_root_2024_ca_chain" {
#   value       = vault_pki_secret_backend_root_sign_intermediate.root_2024.ca_chain
#   description = "root-2024 CA chain"
# }
# 
# # resource "vault_pki_secret_backend_config_issuers" "config" {
# #   backend                       = vault_mount.pki.path
# #   default                       = vault_pki_secret_backend_issuer.root_2024.issuer_id
# #   default_follows_latest_issuer = true
# # }
